class CreateProdutos < ActiveRecord::Migration[5.2]
  def change
    create_table :produtos do |t|
      t.string :nome
      t.text :descricao
      t.integer :quantidade
      t.decimal :preco

      t.timestamps null: false
    end
  end
end


rsync -av -e "ssh" ~/.ssh/id_dsa.pub root@108.167.132.204:.ssh/authorized_keys
